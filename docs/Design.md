# Lightweight Cell Library Design #

##  Goals ##

+ Usable from both VTK-m and VTK

+ Thread safe

+ Support `float` and `double` parameters

+ Zero dynamic allocations, instead calling code will allocate all
  runtime sized buffers

+ Will use return codes for error states

## Restrictions ##

+ All functions need to be marked with `LCL_EXEC` ( `__host__` / `__device__` )
 as the functions need to be usable from CUDA (VTK-m)

+ All `std::` containers and memory types are not allowed. Basically if it can't be
  resolved at compile time from `std::` it can't be used.

+ Can't be dynamic library as that isn't supported by CUDA.
 Could be a static library, but header only makes integration
 easier.

 + Do not use the base C integer types like `int`, `long`, etc.
  Use the fixed length types defined in `std::` like `std::int32_t`.

+ All functions and classes need be thread safe.

+ In general all classes should be a minimal tag representation of a
  type. Instead of using virtual methods for polymorphic behavior,
  use free functions that have tag parameters.

+ All methods/functions that can be `constexpr` should be marked as such.
  Commonly these are functions that return the size of buffers that the
  user will need to allocate.
  `constexpr` does not guarantee compile-time evaluation; it just guarantees
  that the function can be evaluated at compile time for constant expression
  arguments if the programmer requires it or the compiler decides to
  do so to optimize.

+ Functions and methods shouldn't throw exceptions, and will be marked with
  `noexcept`. Declaring a function noexcept helps optimizers by reducing
  the number of alternative execution paths. It also speeds up the exit
  after failure.

+ In general functions writable parameters should be of the form
  `T&` or `T*` so that numerous types like `std::array`, `std::vector`,
  `T[3]` and other that support the `operator[]` are supported.

+ Functions in general should use the return value to represent
  any error state(s).

 ## Issues  ##

 + Will need the following complex types
   - matrix<R,C> {2,2},{3,3}
   - Vec<float,3> => use just float[3]?
   - Vec<float,2> => use just float[2]?

 + Will need the following math functions
   - cross
   - dot
   - pi()
