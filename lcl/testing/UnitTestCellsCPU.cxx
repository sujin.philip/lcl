//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.md for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//============================================================================
#include "TestingCells.h"

#include <condition_variable>
#include <functional>
#include <mutex>
#include <thread>

namespace
{

class MultithreadedExecutor
{
public:
  MultithreadedExecutor()
  {
    auto numThreads = std::thread::hardware_concurrency();
    this->ThreadPool.reserve(numThreads);
    for (size_t i = 0; i < numThreads; ++i)
    {
      this->ThreadPool.emplace_back(threadFunction, &this->State, i, numThreads);
    }
  }

  ~MultithreadedExecutor()
  {
    this->postTask(nullptr, 0);
    for(auto& thread : this->ThreadPool)
    {
      thread.join();
    }
  }

  template <typename Functor, typename... Args>
  void run(const Functor& func, size_t count, Args&&... args) const
  {
    this->postTask([func, &args...](size_t i){ func(i, args...); }, count);
  }

  template <typename T>
  static const T* in(const std::vector<T>& para)
  {
    return para.data();
  }

  template <typename T>
  static T* out(std::vector<T>& para)
  {
    return para.data();
  }

private:
  struct ThreadPoolState
  {
    std::mutex TaskLock;
    std::condition_variable TaskReady;
    unsigned TaskId = 0;
    std::function<void(std::size_t)> Task;
    size_t NumInvocations;

    std::mutex SyncLock;
    std::condition_variable Sync;
    unsigned int SyncCounter = 0;
  };

  static void threadFunction(ThreadPoolState* state, size_t threadId, size_t numThreads)
  {
    bool done = false;
    unsigned taskId = 0;

    while (!done)
    {
      std::unique_lock<std::mutex> tl(state->TaskLock);
      while (taskId == state->TaskId)
      {
        state->TaskReady.wait(tl);
      }

      taskId = state->TaskId;
      auto task = state->Task;
      auto numInvocations = state->NumInvocations;
      tl.unlock();

      if (task)
      {
        for (size_t i = threadId; i < numInvocations; i += numThreads)
        {
          task(i);
        }
      }
      else
      {
        done = true;
      }

      std::unique_lock<std::mutex> sl(state->SyncLock);
      if (++state->SyncCounter == numThreads)
      {
        sl.unlock();
        state->Sync.notify_all();
      }
    }
  }

  void postTask(const std::function<void(std::size_t)>& task, size_t numInvocations) const
  {
    std::unique_lock<std::mutex> tl(this->State.TaskLock);
    this->State.Task = task;
    this->State.NumInvocations = numInvocations;
    ++this->State.TaskId;
    tl.unlock();
    this->State.TaskReady.notify_all();

    std::unique_lock<std::mutex> sl(this->State.SyncLock);
    while (this->State.SyncCounter < this->ThreadPool.size())
    {
      this->State.Sync.wait(sl);
    }
    this->State.SyncCounter = 0;
  }

  std::vector<std::thread> ThreadPool;
  mutable ThreadPoolState State;
};

} // anonymous namespace

int main()
{
  lcl::testing::TestCells(MultithreadedExecutor());
  return 0;
}
